#TP ARCHITECTURE EVENT BUS

L’objectif de ce TP est d’implémenter un simulateur de paiements pour une banque. Le simulateur génère des paiements d’un montant aléatoire toutes les X secondes afin d’être traité par un agent (automate). 

Le nombre de transactions par minute peut atteindre plusieurs millions et leur traitement par l’agent peut prendre plusieurs secondes par traitement. 

L’objectif de ce TP est donc d’implémenter une architecture résiliente aux pannes afin de ne pas interrompre l'émission des paiements (la moindre panne peut couter cher), et scalable afin de permettre d’augmenter la capacité de traitement à la demande.

Pour cette simulation, le projet contient deux fichiers :
●	emitter.js : Ce module génère des transactions d’un montant aléatoire toutes les X secondes (paramétrable). 
●	agent.js : Ce module est chargé de traiter les paiements. 

La simulation peut être lancée via la commande suivante (par défaut, l’interval vaut 100ms) :

``` bash
node emitter.js <interval_en_ms>
```

##Publication d’évènements

L’objectif de cet exercice est de découpler l'émission de paiements et leur traitement (emitter et agent) en utilisant un event bus (RabbitMQ). Les événements devront être publiés dans une file d’attente RabbitMQ (en utilisant le protocol standard AMQP).

Modifier le code de emitter.js afin de publier les transactions générées sur une file RabbitMQ (queues). Pour cela, utiliser la libraire NodeJS amqplib et implémenter les étapes suivantes :
●	Connexion au serveur RabbitMQ 
●	Création du paiement
●	Publication de la transaction sur la file « transactions » 

L’event bus RabbitMQ n’accepte que des messages au format chaine de caractère. Le message doit donc etre transformé en utilisant la synthaxe suivante Buffer.from(JSON.stringify(…))

Vérifier que les transactions sont bien publiés en vous connectant à l’interface web RabbitMQ http://localhost:15672

## Souscription à des évènements

Modifier le code de agent.js afin de lire les transactions sur la file « transactions » pour traitement. ). Pour cela, utiliser la libraire NodeJS amqplib et implémenter les étapes suivantes :
●	Connexion au serveur RabbitMQ 
●	Souscription à la file « transactions »
●	Traitement du paiement (en 3 secondes)
●	Envoie de la confirmation de traitement (ack)

Exemple connexion au serveur RabbitMQ en utilisant la librairie amqlib : 
``` javascript
    // Connexion
    const connection = await amqplib.connect('amqp://guest:guest@localhost:5672');
    
    // Création du canal
    const channel = await connection.createChannel();
    
    //Création de la queue
    let queue = await channel.assertQueue("transactions", {durable: true});
```

Lancer plusieurs agents en parallèle et s’assurer que les transactions sont bien consommés en vous connectant à l’interface web RabbitMQ http://localhost:15672

## Transactions invalides

Certaines transactions corrompues sont générées avec des montants invalides (-1). Ces transactions doivent être ignorées et rejetées de la file d’attente. Modifier le code de « agent.js » pour prendre en compte ce cas.

## Notifications

Après traitements, une notification doit être envoyée à l'émetteur du paiement afin de l’avertir que la transaction a bien été effectuée. La même notification est envoyée via SMS et MAIL. Chaque type d’envoi est traité par un agent différent.

Créer deux modules nommés agent_sms_notifier.js et agent_mail_notifier.js
Le premier devra souscrire à une file nommée notifications_sms et le second à une file nommée notifications_mail. 

Pour la simulation, les modules se contenteront d’afficher un message du type :

[MAIL] Mail envoyé à test@test.fr avec succès (<contenu_du_message>)
ET
[SMS] SMS envoyé au 06 52 52 52 52 avec succès (<contenu_du_message>)

Modifier le code de agent.js pour qu’une seule notification soit publiée à la fois sur la file notifications_sms et notifications_mail (utiliser les exchange en mode broadcast)

La notification doit-être publiée au format suivant :

{
	message : ‘Bonjour John Doe, votre paiement de 1500€ a bien été effectué’,
	account_id : ‘85a552daa65b6125’,
	mail : ‘john.doe@test.de’,
	tel : ‘+33 6 42 59 96 52’
}

Lancer la simulation complète et vérifier que tous les messages sont bien consommés de bout en bout.

Lancer plusieurs agents afin de s’assurer que la solution implémentée est scalable.

[OPTIONEL]

Créer une interface web permettant de lancer la simulation. L’interface devra permettre :
•	D’afficher le nombre de paiments en cours
•	Paramétrer le nombre de paiements a simuler
•	Paramétrer la durée entre chaque emission de paiement
•	Lancer un ou plusieurs emetteurs de paiement
•	Arreter un ou plusieurs emetteurs de paiement
•	Lancer un ou plusieurs agents de traitement
•	Arreter un ou plusieurs agents de traitement


Pour lancer un serveur HTTP en nodejs, utiliser le module express : https://expressjs.com/fr/starter/hello-world.html

Pour lancer un processus node, utiliser le module child_process : https://nodejs.org/api/child_process.html#child_processforkmodulepath-args-options

